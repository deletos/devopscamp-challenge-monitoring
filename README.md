# devopscamp - adidas OPS challenge monitoring

### Challenge
Your mission, should you choose to accept it, is to create a monitoring setup.

### Specifications
- Setup shall monitor http://devopsws.crm.aws.ds.3stripes.net/
- admin account wordpress: devopscamp/trustinautomation
- In case webpage service is impacted alerts shall be triggered to "devopscampadidas@gmail.com" / pw: letmein
- any kind of monitoring tool can be used
- any kind of service can be added to monitoring
- Time gap between impact and receiving message shall be less than 3 min

#### Optional
- Configs to be stored in this gitlab project
- monitor second page http://www.adidas.de 
- Second alert channel beside mail (e.g. slack/text notification)
- collecting and visualising performance data
- distributed monitoring(different datacenters)
- more features

